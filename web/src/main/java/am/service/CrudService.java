package am.service;

import java.util.List;

public interface CrudService<Request, Response> {

    Response create(Request Request);

    Response read(Long id);

    Response update(Long id, Request request);

    Response delete(Long id);

    List<Response> findAll();

}
