package am.service;

import am.dto.user.LoginPayload;
import am.dto.user.RegisterPayload;
import am.dto.user.UserResponseDTO;

public interface AuthService {

    UserResponseDTO register(RegisterPayload registerPayload);

    UserResponseDTO login(LoginPayload loginPayload);

}
