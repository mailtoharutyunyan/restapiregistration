package am.service;

import am.dto.question.QuestionDTO;
import am.service.CrudService;

import java.util.List;

public interface QuestionService extends CrudService<QuestionDTO, QuestionDTO> {

    @Override
    QuestionDTO create(QuestionDTO questionDTO);

    @Override
    QuestionDTO read(Long id);

    @Override
    QuestionDTO update(Long id, QuestionDTO questionDTO);

    @Override
    QuestionDTO delete(Long id);

    @Override
    List<QuestionDTO> findAll();
}
