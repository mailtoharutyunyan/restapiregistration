package am.service.impl;

import am.dto.question.QuestionDTO;
import am.entity.question.Question;
import am.repository.QuestionRepository;
import am.service.QuestionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    //TODO need to add orika

    private final QuestionRepository questionRepository;

    @Override
    public QuestionDTO create(QuestionDTO questionDTO) {
        Question question = new Question();
        question.setName(questionDTO.getName());
        question.setCount(questionDTO.getCount());
        return questionDTO;
    }

    @Override
    public QuestionDTO read(Long id) {
        return null;
    }

    @Override
    public QuestionDTO update(Long id, QuestionDTO questionDTO) {
        return null;
    }

    @Override
    public QuestionDTO delete(Long id) {
        return null;
    }

    @Override
    public List<QuestionDTO> findAll() {
        List<Question> list = questionRepository.findAll();
        List<QuestionDTO> questionDTOList = new ArrayList<>();
        BeanUtils.copyProperties(list, questionDTOList);
        return questionDTOList;
    }
}
