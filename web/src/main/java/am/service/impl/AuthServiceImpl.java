package am.service.impl;

import am.security.JwtTokenProvider;
import am.dto.user.LoginPayload;
import am.dto.user.RegisterPayload;
import am.dto.user.UserResponseDTO;
import am.entity.user.Role;
import am.entity.user.Status;
import am.entity.user.User;
import am.exception.CustomException;
import am.repository.UserRepository;
import am.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public UserResponseDTO login(LoginPayload loginPayload) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginPayload.getUsername(),
                            loginPayload.getPassword()));
            String token = jwtTokenProvider.createToken(loginPayload.getUsername(),
                    userRepository.findByUsername(loginPayload.getUsername()).getRoles());
            User userFromDb = userRepository.findByUsername(loginPayload.getUsername());

            UserResponseDTO response = new UserResponseDTO();
            response.setId(userFromDb.getId());
            response.setEmail(userFromDb.getEmail());
            response.setUsername(userFromDb.getUsername());
            response.setToken(token);
            return response;

        } catch (AuthenticationException e) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public UserResponseDTO register(RegisterPayload registerPayload) {
        try {
            if (!userRepository.existsByUsername(registerPayload.getUsername())) {
                User newUser = new User();
                newUser.setUsername(registerPayload.getUsername());
                newUser.setPassword(passwordEncoder.encode(registerPayload.getPassword()));
                newUser.setEmail(registerPayload.getEmail());
                List<Role> roles = new ArrayList<>();
                roles.add(Role.ROLE_CLIENT);
                newUser.setRoles(roles);
                newUser.setStatus(Status.ACTIVE);
                User savedUser = userRepository.save(newUser);
                String token = jwtTokenProvider.createToken(newUser.getUsername(), newUser.getRoles());
                /*User Response Part*/
                UserResponseDTO response = new UserResponseDTO();
                response.setId(savedUser.getId());
                response.setEmail(savedUser.getEmail());
                response.setUsername(savedUser.getUsername());
                response.setToken(token);
                return response;
            } else {
                throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
