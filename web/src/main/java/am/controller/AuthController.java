package am.controller;

import am.dto.user.LoginPayload;
import am.dto.user.RegisterPayload;
import am.dto.user.UserResponseDTO;
import am.service.AuthService;
import am.validator.RequestFieldsValidator;
import lombok.AllArgsConstructor;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/auth")
@AllArgsConstructor
public class AuthController {

    private final RequestFieldsValidator requestFieldsValidator;
    private final AuthService authService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hello() {
        return "Hello test";
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public UserResponseDTO register(@Valid @RequestBody RegisterPayload registerPayload, Errors errors) {
        requestFieldsValidator.validate(errors);
        return authService.register(registerPayload);
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public UserResponseDTO login(@Valid @RequestBody LoginPayload loginPayload, Errors errors) {
        requestFieldsValidator.validate(errors);
        return authService.login(loginPayload);
    }

}
