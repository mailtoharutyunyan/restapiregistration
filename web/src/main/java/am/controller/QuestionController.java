package am.controller;

import am.dto.question.QuestionDTO;
import am.entity.question.Question;
import am.service.QuestionService;
import am.validator.RequestFieldsValidator;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/test")
public class QuestionController {

    private final RequestFieldsValidator requestFieldsValidator;
    private final QuestionService questionService;

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    public QuestionDTO save(@Valid @RequestBody QuestionDTO questionDTO, Errors errors) {
        requestFieldsValidator.validate(errors);
        return questionService.create(questionDTO);
    }

}
