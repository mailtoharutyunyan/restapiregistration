package am.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import am.entity.user.User;

import javax.transaction.Transactional;

public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByUsername(String username);

    User findByUsername(String username);

    @Transactional
    User deleteByUsername(String username);

}