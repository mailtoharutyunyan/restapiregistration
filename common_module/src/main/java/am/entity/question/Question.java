package am.entity.question;

import am.entity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Question extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "count")
    private Integer count;


}
