package am.entity.user;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}