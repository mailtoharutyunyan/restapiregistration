package am.advice;

import am.error.ErrorResponseDto;
import am.exception.BaseException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<ErrorResponseDto> handleBaseException(BaseException ex) {
        return new ResponseEntity<>(new ErrorResponseDto(
                ex.getErrorCode().getName(),
                ex.getErrors()),
                ex.getErrorCode().getStatus());
    }
}