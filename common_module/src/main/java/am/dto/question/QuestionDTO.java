package am.dto.question;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionDTO {
    private String name;
    private Integer count;
}
