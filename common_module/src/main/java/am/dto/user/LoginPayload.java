package am.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LoginPayload {

    @NotBlank(message = "There is an empty username")
    @NotNull(message = "Username is null")
    private String username;
    @NotBlank(message = "There is an empty password")
    @NotNull(message = "Password is null")
    private String password;

}
