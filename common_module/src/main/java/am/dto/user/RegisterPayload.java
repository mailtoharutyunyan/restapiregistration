package am.dto.user;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RegisterPayload {

    @NotBlank(message = "There is an empty username")
    @NotNull(message = "Username is null")
    private String username;

    @Email
    @NotBlank(message = "There is an empty email")
    @NotNull(message = "Email is null")
    private String email;

    @NotBlank(message = "There is an empty password")
    @NotNull(message = "Password is null")
    private String password;

}
